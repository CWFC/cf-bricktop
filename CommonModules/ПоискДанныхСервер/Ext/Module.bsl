﻿Процедура НайтиСсылкиПоСоответствиюОбъектовБукмекера(Букмекер, ТаблицаПоиска, ЗаменитьВсе = Ложь) Экспорт
	Запрос = Новый Запрос;
	Запрос.УстановитьПараметр("Букмекер", Букмекер);
	Запрос.УстановитьПараметр("ТаблицаПоиска", ТаблицаПоиска.Выгрузить());
	Запрос.УстановитьПараметр("ЗаменитьВсе", ЗаменитьВсе);
	Запрос.Текст =
		"ВЫБРАТЬ
		|	ТаблицаПоиска.НомерСтроки КАК НомерСтроки,
		|	ЛОЖЬ КАК ЕстьСсылки,
		|	ТаблицаПоиска.URL КАК URL,
		|	ТаблицаПоиска.ДатаОбновления КАК ДатаОбновления,
		|	ТаблицаПоиска.Вид КАК Вид,
		|	ВЫБОР
		|		КОГДА &ЗаменитьВсе
		|			ТОГДА ЗНАЧЕНИЕ(Справочник.ГруппыСобытий.ПустаяСсылка)
		|		ИНАЧЕ ТаблицаПоиска.Группа
		|	КОНЕЦ КАК Группа,
		|	ТаблицаПоиска.ГруппаСтрока КАК ГруппаСтрока,
		|	ВЫБОР
		|		КОГДА &ЗаменитьВсе
		|			ТОГДА ЗНАЧЕНИЕ(Справочник.УчастникиСобытий.ПустаяСсылка)
		|		ИНАЧЕ ТаблицаПоиска.Участник1
		|	КОНЕЦ КАК Участник1,
		|	ТаблицаПоиска.Участник1Строка КАК Участник1Строка,
		|	ВЫБОР
		|		КОГДА &ЗаменитьВсе
		|			ТОГДА ЗНАЧЕНИЕ(Справочник.УчастникиСобытий.ПустаяСсылка)
		|		ИНАЧЕ ТаблицаПоиска.Участник2
		|	КОНЕЦ КАК Участник2,
		|	ТаблицаПоиска.Участник2Строка КАК Участник2Строка,
		|	ТаблицаПоиска.Дата КАК Дата,
		|	ТаблицаПоиска.К1 КАК К1,
		|	ТаблицаПоиска.КХ КАК КХ,
		|	ТаблицаПоиска.К2 КАК К2
		|ПОМЕСТИТЬ ВТТаблицаПоиска
		|ИЗ
		|	&ТаблицаПоиска КАК ТаблицаПоиска
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	СоответствиеОбъектовБукмекеров.ТипОбъекта КАК ТипОбъекта,
		|	СоответствиеОбъектовБукмекеров.Представление КАК Представление,
		|	СоответствиеОбъектовБукмекеров.Объект КАК Объект
		|ПОМЕСТИТЬ ВТСоответствияБукмекера
		|ИЗ
		|	РегистрСведений.СоответствиеОбъектовБукмекеров КАК СоответствиеОбъектовБукмекеров
		|ГДЕ
		|	СоответствиеОбъектовБукмекеров.Букмекер = &Букмекер
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ВТТаблицаПоиска.НомерСтроки КАК НомерСтроки,
		|	ВТТаблицаПоиска.ЕстьСсылки КАК ЕстьСсылки,
		|	ВТТаблицаПоиска.URL КАК URL,
		|	ВТТаблицаПоиска.ДатаОбновления КАК ДатаОбновления,
		|	ВТТаблицаПоиска.Вид КАК Вид,
		|	ВЫБОР
		|		КОГДА ВТТаблицаПоиска.Группа = ЗНАЧЕНИЕ(Справочник.ГруппыСобытий.ПустаяСсылка)
		|			ТОГДА ЕСТЬNULL(ВТГруппыСобытий.Объект, ЗНАЧЕНИЕ(Справочник.ГруппыСобытий.ПустаяСсылка))
		|		ИНАЧЕ ВТТаблицаПоиска.Группа
		|	КОНЕЦ КАК Группа,
		|	ВТТаблицаПоиска.ГруппаСтрока КАК ГруппаСтрока,
		|	ВЫБОР
		|		КОГДА ВТТаблицаПоиска.Участник1 = ЗНАЧЕНИЕ(Справочник.УчастникиСобытий.ПустаяСсылка)
		|			ТОГДА ЕСТЬNULL(ВТУчастникиСобытий1.Объект, ЗНАЧЕНИЕ(Справочник.УчастникиСобытий.ПустаяСсылка))
		|		ИНАЧЕ ВТТаблицаПоиска.Участник1
		|	КОНЕЦ КАК Участник1,
		|	ВТТаблицаПоиска.Участник1Строка КАК Участник1Строка,
		|	ВЫБОР
		|		КОГДА ВТТаблицаПоиска.Участник2 = ЗНАЧЕНИЕ(Справочник.УчастникиСобытий.ПустаяСсылка)
		|			ТОГДА ЕСТЬNULL(ВТУчастникиСобытий2.Объект, ЗНАЧЕНИЕ(Справочник.УчастникиСобытий.ПустаяСсылка))
		|		ИНАЧЕ ВТТаблицаПоиска.Участник2
		|	КОНЕЦ КАК Участник2,
		|	ВТТаблицаПоиска.Участник2Строка КАК Участник2Строка,
		|	ВТТаблицаПоиска.Дата КАК Дата,
		|	ВТТаблицаПоиска.К1 КАК К1,
		|	ВТТаблицаПоиска.КХ КАК КХ,
		|	ВТТаблицаПоиска.К2 КАК К2
		|ПОМЕСТИТЬ ВТРезультатПоиска
		|ИЗ
		|	ВТТаблицаПоиска КАК ВТТаблицаПоиска
		|		ЛЕВОЕ СОЕДИНЕНИЕ ВТСоответствияБукмекера КАК ВТГруппыСобытий
		|		ПО (ВТГруппыСобытий.ТипОбъекта = ЗНАЧЕНИЕ(Перечисление.ТипыОбъектовБукмекеров.ГруппаСобытий))
		|			И ВТТаблицаПоиска.ГруппаСтрока = ВТГруппыСобытий.Представление
		|		ЛЕВОЕ СОЕДИНЕНИЕ ВТСоответствияБукмекера КАК ВТУчастникиСобытий1
		|		ПО (ВТУчастникиСобытий1.ТипОбъекта = ЗНАЧЕНИЕ(Перечисление.ТипыОбъектовБукмекеров.УчастникСобытия))
		|			И ВТТаблицаПоиска.Участник1Строка = ВТУчастникиСобытий1.Представление
		|		ЛЕВОЕ СОЕДИНЕНИЕ ВТСоответствияБукмекера КАК ВТУчастникиСобытий2
		|		ПО (ВТУчастникиСобытий2.ТипОбъекта = ЗНАЧЕНИЕ(Перечисление.ТипыОбъектовБукмекеров.УчастникСобытия))
		|			И ВТТаблицаПоиска.Участник2Строка = ВТУчастникиСобытий2.Представление
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|ВЫБРАТЬ
		|	ВТРезультатПоиска.НомерСтроки КАК НомерСтроки,
		|	ВЫБОР
		|		КОГДА ВТРезультатПоиска.Группа = ЗНАЧЕНИЕ(Справочник.ГруппыСобытий.ПустаяСсылка)
		|				ИЛИ ВТРезультатПоиска.Участник1 = ЗНАЧЕНИЕ(Справочник.УчастникиСобытий.ПустаяСсылка)
		|				ИЛИ ВТРезультатПоиска.Участник2 = ЗНАЧЕНИЕ(Справочник.УчастникиСобытий.ПустаяСсылка)
		|			ТОГДА ЛОЖЬ
		|		ИНАЧЕ ИСТИНА
		|	КОНЕЦ КАК ЕстьСсылки,
		|	ВТРезультатПоиска.URL КАК URL,
		|	ВТРезультатПоиска.ДатаОбновления КАК ДатаОбновления,
		|	ВТРезультатПоиска.Вид КАК Вид,
		|	ВТРезультатПоиска.Группа КАК Группа,
		|	ВТРезультатПоиска.ГруппаСтрока КАК ГруппаСтрока,
		|	ВТРезультатПоиска.Участник1 КАК Участник1,
		|	ВТРезультатПоиска.Участник1Строка КАК Участник1Строка,
		|	ВТРезультатПоиска.Участник2 КАК Участник2,
		|	ВТРезультатПоиска.Участник2Строка КАК Участник2Строка,
		|	ВТРезультатПоиска.Дата КАК Дата,
		|	ВТРезультатПоиска.К1 КАК К1,
		|	ВТРезультатПоиска.КХ КАК КХ,
		|	ВТРезультатПоиска.К2 КАК К2,
		|	ЕСТЬNULL(ИсторияЛинийСрезПоследних.К1, 0) КАК К1История,
		|	ЕСТЬNULL(ИсторияЛинийСрезПоследних.КХ, 0) КАК КХИстория,
		|	ЕСТЬNULL(ИсторияЛинийСрезПоследних.К2, 0) КАК К2История
		|ИЗ
		|	ВТРезультатПоиска КАК ВТРезультатПоиска
		|		ЛЕВОЕ СОЕДИНЕНИЕ РегистрСведений.ИсторияЛиний.СрезПоследних(, Букмекер = &Букмекер) КАК ИсторияЛинийСрезПоследних
		|		ПО ВТРезультатПоиска.Вид = ИсторияЛинийСрезПоследних.Вид
		|			И ВТРезультатПоиска.Группа = ИсторияЛинийСрезПоследних.Группа
		|			И ВТРезультатПоиска.Дата = ИсторияЛинийСрезПоследних.Дата
		|			И ВТРезультатПоиска.Участник1 = ИсторияЛинийСрезПоследних.Участник1
		|			И ВТРезультатПоиска.Участник2 = ИсторияЛинийСрезПоследних.Участник2
		|
		|УПОРЯДОЧИТЬ ПО
		|	НомерСтроки
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|УНИЧТОЖИТЬ ВТТаблицаПоиска
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|УНИЧТОЖИТЬ ВТСоответствияБукмекера
		|;
		|
		|////////////////////////////////////////////////////////////////////////////////
		|УНИЧТОЖИТЬ ВТРезультатПоиска";
	Выборка = Запрос.Выполнить().Выбрать();
	ТаблицаПоиска.Очистить();
	Пока Выборка.Следующий() Цикл 
		НоваяСтрока = ТаблицаПоиска.Добавить();
		ЗаполнитьЗначенияСвойств(НоваяСтрока, Выборка);
	КонецЦикла;	
КонецПроцедуры